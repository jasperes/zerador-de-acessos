# -*- mode: python -*-
a = Analysis(['projects\\zera\\zera.py'],
             pathex=['C:\\Documents and Settings\\Usuario\\Desktop\\PyInstaller-2.1\\zera\\projects\\zera\\compiled'],
             hiddenimports=[],
             hookspath=None,
             runtime_hooks=None)
pyz = PYZ(a.pure)
exe = EXE(pyz,
          a.scripts,
          a.binaries + [('fbclient.dll', 'projects\\zera\\include\\fbclient.dll', 'BINARY'), ('zera.ico','projects\\zera\\include\\zera.ico', 'DATA')],
          a.zipfiles,
          a.datas,
          name='zera.exe',
          debug=False,
          strip=None,
          upx=True,
          console=False , icon='projects\\zera\\include\\zera.ico'
          )

#coll = COLLECT(a.binaries, [('fbclient.dll', 'projects\\zera\\include', 'BINARY')])
