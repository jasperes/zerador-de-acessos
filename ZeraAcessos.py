# -*- coding: iso-8859-1 -*-

import sys
import pyodbc
import pymssqlce
#import kinterbasdb
import cx_Oracle
from PySide.QtGui import QApplication, QMainWindow, QWidget, QVBoxLayout, QHBoxLayout, QLabel, QLineEdit, QTextEdit, QPushButton, QComboBox
from PySide.QtGui import QFileDialog, QMessageBox, QProgressDialog, QIcon, QStandardItemModel, QStandardItem, QTreeView
from PySide.QtCore import Qt, QCoreApplication
from datetime import date
from threading import Thread
from socket import gethostname
from zipfile import ZipFile
from os import mkdir, remove, system
from os.path import exists

#sswsup-0086
try:
    connector = pyodbc.connect(r'DRIVER={SQL Server};SERVER=sswsup-0086\sqlexpress;DATABASE=ZeradorDeAcessos;UID=sa;PWD=_43690;')
    mainDB = connector.cursor()
    e = None
except pyodbc.OperationalError as err:
    e = ['OperationalError', err]
except pyodbc.DataError as err:
    e = ['DataError', err]
except pyodbc.IntegrityError as err:
    e = ['IntegrityError', err]
except pyodbc.ProgrammingError as err:
    e = ['ProgrammingError', err]
except pyodbc.NotSupportedError as err:
    e = ['NotSupportedError', err]
except pyodbc.DatabaseError as err:
    e = ['DataBaseError', err]
except pyodbc.Error as err:
    e = ['Error', err]
except:
    e = ['Erro!', 'erro inesperado']
    
class MainWindow(QMainWindow):
    
    def __init__(self, parent=None):

        super(MainWindow, self).__init__(parent)

        self.setWindowTitle('Zera Acessos')
        self.setWindowIcon(QIcon("zera.ico"))
        self.setGeometry(500, 200, 400, 400)
        #self.setMinimumSize(200, 300)
        self.setCentralWidget(MainWidget(self))

class MainWidget(QWidget):
    
    def __init__(self, parent):
        super(MainWidget, self).__init__(parent)
        
        title = QLabel("<font size=10><b>Zera Acessos</b></font>")

        cliente = QLabel('Cliente (Automático)')
        self.cliente = QLineEdit()
        cnpj = QLabel('CNPJ (Automático)')
        self.cnpj = QLineEdit()
        revenda = QLabel('Revenda')
        self.revenda = QComboBox()
        sistema = QLabel('Sistema')
        self.sistema = QComboBox()
        self.clientes_zerados = QPushButton("Clientes zerados")
        
        tipo_banco = QLabel('Tipo de banco')
        self.tipo_banco = QComboBox()
        server = QLabel('Servidor', )
        self.server = QLineEdit()
        user = QLabel('Usuário')
        self.user = QLineEdit()
        pssw = QLabel('Senha')
        self.pssw = QLineEdit()
        banco = QLabel('Banco de dados')
        self.banco = QLineEdit()
        self.btn_banco = QPushButton('...')

        motivo = QLabel('Motivo')
        self.motivo = QTextEdit()

        btn_ok = QPushButton('Zerar')
        btn_cancel = QPushButton('Fechar')

        self.sistema.addItem('Ponto Secullum 4')
        self.sistema.addItem('Secullum Acesso.Net')
        self.sistema.addItem('Secullum Academia.Net')
        self.sistema.addItem('Secullum Clube.Net')
        self.sistema.addItem('Secullum Mini Folha.Net')
        self.sistema.addItem('Secullum Estacionamento.Net')


        self.cliente.setMaxLength(150)
        self.cliente.setReadOnly(1)

        self.cnpj.setInputMask("00.000.000/0000-00")
        self.cnpj.setReadOnly(1)
        self.cnpj.setMaxLength(18)

        self.revenda.setEditable(1)
        
        self.pssw.setEchoMode(QLineEdit.Password)

        self.clientes_zerados.setFlat(1)

        box = QVBoxLayout()
        box1 = QHBoxLayout()
        boxtlt = QHBoxLayout()
        boxCdt = QVBoxLayout()
        boxDB = QHBoxLayout()
        boxCnx = QVBoxLayout()
        boxBtn = QHBoxLayout()
        boxMtv = QVBoxLayout()

        boxtlt.addStretch(1)
        boxtlt.addWidget(QLabel(''))
        boxtlt.addStretch(1)
        boxtlt.addWidget(title)
        boxtlt.addStretch(2)
        boxtlt.addWidget(QLabel(''))

        boxCdt.addStretch(1)
        boxCdt.addWidget(cliente)
        boxCdt.addWidget(self.cliente)
        boxCdt.addWidget(cnpj)
        boxCdt.addWidget(self.cnpj)
        boxCdt.addWidget(revenda)
        boxCdt.addWidget(self.revenda)
        boxCdt.addWidget(sistema)
        boxCdt.addWidget(self.sistema)
        boxCdt.addStretch(25)
        boxCdt.addWidget(self.clientes_zerados)

        boxDB.addStretch(1)
        boxDB.addWidget(self.banco)
        boxDB.addWidget(self.btn_banco)

        boxCnx.addWidget(tipo_banco)
        boxCnx.addWidget(self.tipo_banco)
        boxCnx.addWidget(server)
        boxCnx.addWidget(self.server)
        boxCnx.addWidget(user)
        boxCnx.addWidget(self.user)
        boxCnx.addWidget(pssw)
        boxCnx.addWidget(self.pssw)
        boxCnx.addWidget(banco)
        boxCnx.addLayout(boxDB)
        boxCnx.addStretch(1)

        boxMtv.addStretch(1)
        boxMtv.addWidget(motivo)
        boxMtv.addWidget(self.motivo)

        boxBtn.addStretch(1)
        boxBtn.addWidget(btn_ok)
        boxBtn.addWidget(btn_cancel)
        boxBtn.addStretch(1)

        box1.addStretch(1)
        box1.addLayout(boxCdt)
        box1.addLayout(boxCnx)

        box.addLayout(boxtlt)
        box.addLayout(box1)
        box.addLayout(boxMtv)
        box.addLayout(boxBtn)
        self.setLayout(box)

        self.btn_banco.clicked.connect(self.dlgDataBase)
        self.sistema.currentIndexChanged.connect(self.insereDataBaseList)
        self.tipo_banco.currentIndexChanged.connect(self.onChangeTipoBanco)
        btn_ok.clicked.connect(self.zerar)
        btn_cancel.clicked.connect(QCoreApplication.instance().quit)
        self.clientes_zerados.clicked.connect(self.layoutWindow)

        self.onChangeTipoBanco()
        self.insereDataBaseList()
        self.insereRevendas()
        self.limpaCampos()

    def insereDataBaseList(self):

        if self.sistema.currentText() == 'Ponto Secullum 4':

            self.tipo_banco.clear()
            self.tipo_banco.addItem('Access')
            self.tipo_banco.addItem('SQL Server')
            self.tipo_banco.addItem('Oracle')
            
        else:

            self.tipo_banco.clear()
            self.tipo_banco.addItem('SQL Server CE')
            self.tipo_banco.addItem('SQL Server')
            self.tipo_banco.addItem('Firebird')
            self.tipo_banco.addItem('Oracle')

    def insereRevendas(self):

        self.revenda.clear()
        self.revenda.addItem('')

        for rev in (mainDB.execute("select distinct revenda from acessos_zerados")).fetchall():
            self.revenda.addItem(rev[0])

    def limpaCampos(self):

        self.cliente.clear()
        self.cnpj.clear()
        self.motivo.clear()

    def onChangeTipoBanco(self):

        self.server.clear()
        self.user.clear()
        self.pssw.clear()
        self.banco.clear()

        if self.tipo_banco.currentText() == 'Access':

            self.server.setReadOnly(1)
            self.user.setReadOnly(1)
            self.pssw.setReadOnly(1)
            self.banco.setReadOnly(0)
            self.btn_banco.setFlat(0)
            self.banco.insert('psec.dat')
            self.filter = ("Access (*.dat);;Todos (*.*)")

        elif self.tipo_banco.currentText() == 'SQL Server CE':

            self.server.setReadOnly(1)
            self.user.setReadOnly(1)
            self.pssw.setReadOnly(1)
            self.banco.setReadOnly(0)
            self.btn_banco.setFlat(0)
            self.filter = ("SQL CE (*.sdf);;Todos (*.*)")
            
        elif self.tipo_banco.currentText() == 'Firebird':

            self.server.setReadOnly(0)
            self.server.insert("localhost")
            self.user.setReadOnly(0)
            self.user.insert("sysdba")
            self.pssw.setReadOnly(0)
            self.pssw.insert("masterkey")
            self.banco.setReadOnly(0)
            self.btn_banco.setFlat(0)
            self.filter = ("Firebird (*.fdb);;Todos (*.*)")

        elif self.tipo_banco.currentText() == 'SQL Server':

            self.server.setReadOnly(0)
            self.server.insert(".\sqlexpress")
            self.user.setReadOnly(0)
            self.user.insert("sa")
            self.pssw.setReadOnly(0)
            self.pssw.insert("_43690")
            self.banco.setReadOnly(0)
            self.btn_banco.setFlat(1)
            self.filter = 'False'

        elif self.tipo_banco.currentText() == 'Oracle':
            
            self.server.setReadOnly(0)
            self.user.setReadOnly(0)
            self.pssw.setReadOnly(0)
            self.banco.setReadOnly(1)
            self.btn_banco.setFlat(1)
            self.filter = 'False'

        if self.sistema.currentText() == 'Ponto Secullum 4':
            if self.tipo_banco.currentText() == 'SQL Server':
                self.banco.insert('PontoSecullum4')
        elif self.sistema.currentText() == 'Secullum Academia.Net':
            if self.tipo_banco.currentText() == 'SQL Server CE':
                self.banco.insert('SecullumAcademiaNet.sdf')
            elif self.tipo_banco.currentText() == 'Firebird':
               self.banco.insert('SecullumAcademiaNet.fdb')
            elif self.tipo_banco.currentText() == 'SQL Server':
                self.banco.insert('SecullumAcademiaNet')
        elif self.sistema.currentText() == 'Secullum Acesso.Net':
            if self.tipo_banco.currentText() == 'SQL Server CE':
                self.banco.insert('SecullumAcessoNet.sdf')
            elif self.tipo_banco.currentText() == 'Firebird':
                self.banco.insert('SecullumAcessoNet.fdb')
            elif self.tipo_banco.currentText() == 'SQL Server':
                self.banco.insert('SecullumAcessoNet')
        elif self.sistema.currentText() == 'Secullum Clube.Net':
            if self.tipo_banco.currentText() == 'SQL Server CE':
                self.banco.insert('SecullumClubeNet.sdf')
            elif self.tipo_banco.currentText() == 'Firebird':
                self.banco.insert('SecullumClubeNet.fdb')
            elif self.tipo_banco.currentText() == 'SQL Server':
                self.banco.insert('SecullumClubeNet')
        elif self.sistema.currentText() == 'Secullum Estacionamento.Net':
            if self.tipo_banco.currentText() == 'SQL Server CE':
                self.banco.insert('SecullumEstacionamentoNet.sdf')
            elif self.tipo_banco.currentText() == 'Firebird':
                self.banco.insert('SecullumEstacionamentoNet.fdb')
            elif self.tipo_banco.currentText() == 'SQL Server':
                self.banco.insert('SecullumEstacionamentoNet')
        elif self.sistema.currentText() == 'Secullum Mini Folha.Net':
            if self.tipo_banco.currentText() == 'SQL Server CE':
                self.banco.insert('SecullumMiniFolhaNet.sdf')
            elif self.tipo_banco.currentText() == 'Firebird':
                self.banco.insert('SecullumMiniFolhaNet.fdb')
            elif self.tipo_banco.currentText() == 'SQL Server':
                self.banco.insert('SecullumMiniFolhaNet')
    
    def dlgDataBase(self):

        if self.filter == 'False':
            return False
        else:
            self.banco.clear()
            self.banco.insert(QFileDialog.getOpenFileName(self, 'Open file', filter=self.filter)[0])

    def zerar(self):

        if (self.revenda.currentText()).strip() == '' or (self.motivo.toPlainText()).strip() == '':
            return QMessageBox.warning(self, "Atenção!", "Preencha os campos corretamente")

        self.progress = QProgressDialog(self)
        self.progress.setWindowModality(Qt.WindowModal)
        self.progress.setMinimum(0)
        self.progress.setMaximum(6)
        self.progress.setWindowTitle("Processando...")
        self.progress.setCancelButton(None)
        self.progress.show()

        self.finished = "nothing"

        if self.finished == "nothing":

            self.progress.setLabelText("Testando conex�o")
            self.progress.setValue(0)

            th = Thread(target=self.testeConexao)
            th.run()

        if self.finished == "nothing":

            self.progress.setLabelText("Verificando empresa")
            self.progress.setValue(1)

            th = Thread(target=self.verificaEmpresa)
            th.run()

        if self.finished == "nothing":
            
            self.progress.setLabelText("Zerando acessos")
            self.progress.setValue(2)

            th = Thread(target=self.zeraAcessos)
            th.run()

        if self.finished == "nothing":

            self.progress.setLabelText("Gerando backup")
            self.progress.setValue(3)

            th = Thread(target=self.salvaBackup())
            th.run()

        if self.finished == "nothing":

            try:
                self.conn.close()
            except:
                pass

            self.progress.setLabelText("Compactando backup")
            self.progress.setValue(4)

            th = Thread(target=self.compactaBackup())
            th.run()

        if self.finished == "nothing":

            self.progress.setLabelText("Finalizando")
            self.progress.setValue(5)

            th = Thread(target=self.finalizaZeraAcessos)
            th.run()

        if self.finished == "nothing":
            self.finished = "suscess"
            self.progress.setValue(6)
        else:
            self.progress.cancel()

        self.limpaCampos()

        if self.finished == "suscess":
            end = QMessageBox.information(self, "Concluido", "Acessos zerado com exito!<br>Backup compactado em <b>C:\\Backup_Zerado\\" + (self.revenda.currentText()).encode('iso-8859-1').decode('iso-8859-1') + "-" + self.data + ".zip</b>")
            system("explorer /select, C:\\Backup_Zerado\\" + (self.revenda.currentText()).encode('iso-8859-1').decode('iso-8859-1') + "-" + self.data + ".zip")
        elif self.finished == "error":
            return QMessageBox.critical(self, "Erro", self.progress_text)
        elif self.finished == "warning":
            return QMessageBox.warning(self, "Aten��o", self.progress_text)
        elif self.finished == "canceled":
            return False
        else:
            return QMessageBox.warning(self, "Ops...", "Fiz merda! Mim chama pelo Skype")

        if self.tipo_banco.currentText() == 'SQL Server' or self.tipo_banco.currentText() == 'Oracle':
            remove(self.zipar)
            
        self.progress.deleteLater()
        self.insereRevendas()

    def testeConexao(self):

        if self.tipo_banco.currentText() == 'Access':
            
            try:
                self.conn = pyodbc.connect(r'DRIVER={Microsoft Access Driver (*.mdb)};DBQ='+self.banco.text())
                self.cur = self.conn.cursor()
            except pyodbc.OperationalError as err:
                self.progress_text = "<font size=5><b> OperationalError </b></font><br><br>"+str(err)
                self.finished = "error"
            except pyodbc.DataError as err:
                self.progress_text = "<font size=5><b> DataError </b></font><br><br>"+str(err)
                self.finished = "error"
            except pyodbc.IntegrityError as err:
                self.progress_text = "<font size=5><b> IntegrityError </b></font><br><br>"+str(err)
                self.finished = "error"
            except pyodbc.ProgrammingError as err:
                self.progress_text = "<font size=5><b> ProgrammingError </b></font><br><br>"+str(err)
                self.finished = "error"
            except pyodbc.NotSupportedError as err:
                self.progress_text = "<font size=5><b> NotSupportedError </b></font><br><br>"+str(err)
                self.finished = "error"
            except pyodbc.DatabaseError as err:
                self.progress_text = "<font size=5><b> DatabaseError </b></font><br><br>"+str(err)
                self.finished = "error"
            except pyodbc.Error as err:
                self.progress_text = "<font size=5><b> Error </b></font><br><br>"+str(err)
                self.finished = "error"
            except:
                self.progress_text = "<font size=5><b> Erro n�o tratado </b></font><br><br>"
                self.finished = "error"

        elif self.tipo_banco.currentText() == 'SQL Server':
            
            try:
                self.conn = pyodbc.connect(r'DRIVER={SQL Server};SERVER='+self.server.text()+';DATABASE='+self.banco.text()+';UID='+self.user.text()+';PWD='+self.pssw.text()+';')
                self.cur = self.conn.cursor()
            except pyodbc.OperationalError as err:
                self.progress_text = "<font size=5><b> OperationalError </b></font><br><br>"+str(err)
                self.finished = "error"
            except pyodbc.DataError as err:
                self.progress_text = "<font size=5><b> DataError </b></font><br><br>"+str(err)
                self.finished = "error"
            except pyodbc.IntegrityError as err:
                self.progress_text = "<font size=5><b> IntegrityError </b></font><br><br>"+str(err)
                self.finished = "error"
            except pyodbc.ProgrammingError as err:
                self.progress_text = "<font size=5><b> ProgrammingError </b></font><br><br>"+str(err)
                self.finished = "error"
            except pyodbc.NotSupportedError as err:
                self.progress_text = "<font size=5><b> NotSupportedError </b></font><br><br>"+str(err)
                self.finished = "error"
            except pyodbc.DatabaseError as err:
                self.progress_text = "<font size=5><b> DatabaseError </b></font><br><br>"+str(err)
                self.finished = "error"
            except pyodbc.Error as err:
                self.progress_text = "<font size=5><b> Error </b></font><br><br>"+str(err)
                self.finished = "error"
            except:
                self.progress_text = "<font size=5><b> Erro n�o tratado </b></font><br><br>"
                self.finished = "error"
            
        elif self.tipo_banco.currentText() == 'SQL Server CE':
            
            self.conn = pymssqlce.connect(self.banco.text())
            self.cur = self.conn.cursor()
        
        elif self.tipo_banco.currentText() == 'Firebird':

            try:
                self.conn = kinterbasdb.connect(host=str(self.server.text()), user=str(self.user.text()), password=str(self.pssw.text()), database=str(self.banco.text()))
                self.cur = self.conn.cursor()
            except kinterbasdb.Warning as err:
                self.progress_text = "<font size=5><b> Warning </b></font><br><br>"+str(err)
                self.finished = "error"
            except kinterbasdb.Error as err:
                self.progress_text = "<font size=5><b> Error </b></font><br><br>"+str(err)
                self.finished = "error"
            except kinterbasdb.InterfaceError as err:
                self.progress_text = "<font size=5><b> InterfaceError </b></font><br><br>"+str(err)
                self.finished = "error"
            except kinterbasdb.DatabaseError as err:
                self.progress_text = "<font size=5><b> DatabaseError </b></font><br><br>"+str(err)
                self.finished = "error"
            except kinterbasdb.DataError as err:
                self.progress_text = "<font size=5><b> DataError </b></font><br><br>"+str(err)
                self.finished = "error"
            except kinterbasdb.OperationalError as err:
                self.progress_text = "<font size=5><b> OperationalError </b></font><br><br>"+str(err)
                self.finished = "error"
            except kinterbasdb.IntegrityError as err:
                self.progress_text = "<font size=5><b> IntegrityError </b></font><br><br>"+str(err)
                self.finished = "error"
            except kinterbasdb.InternalError as err:
                self.progress_text = "<font size=5><b> InternalError </b></font><br><br>"+str(err)
                self.finished = "error"
            except kinterbasdb.ProgrammingError as err:
                self.progress_text = "<font size=5><b> ProgrammingError </b></font><br><br>"+str(err)
                self.finished = "error"
            except kinterbasdb.NotSupportedError as err:
                self.progress_text = "<font size=5><b> NotSuportedError </b></font><br><br>"+str(err)
                self.finished = "error"
            except :
                self.progress_text = "<font size=5><b> Erro n�o tratado! </b></font><br><br>"
                self.finished = "error"
        
        elif self.tipo_banco.currentText() == 'Oracle':
            
            try:
                self.conn = cx_Oracle.connect(str(self.user.text())+'/'+str(self.pssw.text())+'@'+str(self.server.text()))
                self.cur = self.conn.cursor()
            except cx_Oracle.Warning as err:
                self.progress_text = "<font size=5><b> Warning </b></font><br><br>"+str(err)
                self.finished = "error"
            except cx_Oracle.Error as err:
                self.progress_text = "<font size=5><b> Error </b></font><br><br>"+str(err)
                self.finished = "error"
            except cx_Oracle.InterfaceError as err:
                self.progress_text = "<font size=5><b> InterfaceError </b></font><br><br>"+str(err)
                self.finished = "error"
            except cx_Oracle.DatabaseError as err:
                self.progress_text = "<font size=5><b> DatabaseError </b></font><br><br>"+str(err)
                self.finished = "error"
            except cx_Oracle.DataError as err:
                self.progress_text = "<font size=5><b> DataError </b></font><br><br>"+str(err)
                self.finished = "error"
            except cx_Oracle.OperationalError as err:
                self.progress_text = "<font size=5><b> OperationalError </b></font><br><br>"+str(err)
                self.finished = "error"
            except cx_Oracle.IntegrityError as err:
                self.progress_text = "<font size=5><b> IntegrityError </b></font><br><br>"+str(err)
                self.finished = "error"
            except cx_Oracle.InternalError as err:
                self.progress_text = "<font size=5><b> InternalError </b></font><br><br>"+str(err)
                self.finished = "error"
            except cx_Oracle.ProgrammingError as err:
                self.progress_text = "<font size=5><b> ProgrammingError </b></font><br><br>"+str(err)
                self.finished = "error"
            except cx_Oracle.NotSupportedError as err:
                self.progress_text = "<font size=5><b> NotSupportedError </b></font><br><br>"+str(err)
                self.finished = "error"
            except:
                self.progress_text = "<font size=5><b> Erro n�o tratado! </b></font><br><br>"
                self.finished = "error"

    def verificaEmpresa(self):

        if self.tipo_banco.currentText() == "Access":
            self.cur.execute("select top 2 nome, cnpj from empresas")
        elif self.tipo_banco.currentText() == "Firebird":
            self.cur.execute("select first 2 nome, cnpj from empresas")
        else:
            self.cur.execute("select top(2) nome, cnpj from empresas")

        empresas = self.cur.fetchall()
        self.cliente.clear()
        self.cnpj.clear()

        cnpj = "00.000.000/0000-00"

        for empresa in empresas:
            
            if empresa[1] == "00.000.000/0000-00":
                continue
            else:
                nome = empresa[0]
                cnpj = empresa[1]
                break

        if cnpj == "00.000.000/0000-00":
            self.progress_text = "N�o h� empresas neste banco"
            self.finished = "warning"
        else:
            self.cliente.insert(nome)
            self.cnpj.insert(cnpj)

        cnpjs = mainDB.execute("select id from acessos_zerados where cnpj='%s'" % str(self.cnpj.text()))
        l_cnpj = len(cnpjs.fetchall())
        if l_cnpj == 1 and self.cnpj.text() != "03.148.451/0001-69":
            if (QMessageBox.question(self, "Alerta!", "Este banco de dados j� foi zerado. Deseja continuar?\n\n"+
                "AVISO: Se precisar zerar novamente, dever� ser cobrado.\nQualquer d�vida, falar com Pool ou �dson", 
                QMessageBox.Yes | QMessageBox.No)) == QMessageBox.No:
                self.finished = "canceled"
                try:
                    self.conn.close()
                except:
                    pass
        elif l_cnpj > 1 and self.cnpj.text() != "03.148.451/0001-69":
            QMessageBox.warning(self, "Alerta!", "Este banco de dados j� foi zerado 2 vezes e dever� ser cobrado!\n"+
                "Qualquer d�vida, verificar com Pool ou �dson.")
            self.finished = "canceled"
            try:
                self.conn.close()
            except:
                pass
            
    def zeraAcessos(self):
        
        self.cur.execute("update registro_sistema_config set acessos=null")
        self.conn.commit()

    def salvaBackup(self):

        self.data = (date.today()).strftime("%d-%m-%Y")
        diretorio = "C:\\Backup_Zerado\\"
        banco = str(self.banco.text())
        self.backup = (self.cliente.text()).encode('iso-8859-1').decode('iso-8859-1') + "-" + self.data + ".bak"
        self.zipar = ''

        if not exists(diretorio):
            mkdir(diretorio)

        if self.tipo_banco.currentText() == 'Access':
            self.zipar = banco

        elif self.tipo_banco.currentText() == 'SQL Server':
            query = "BACKUP DATABASE %s TO DISK = '%s'" % (banco, diretorio+self.backup)
            try:
                self.cur.execute(query)
                while self.cur.nextset():
                    pass
            except pyodbc.OperationalError as err:
                self.progress_text = "<font size=5><b> OperationalError </b></font><br><br>"+str(err)
                self.finished = "error"
            except pyodbc.DataError as err:
                self.progress_text = "<font size=5><b> DataError </b></font><br><br>"+str(err)
                self.finished = "error"
            except pyodbc.IntegrityError as err:
                self.progress_text = "<font size=5><b> IntegrityError </b></font><br><br>"+str(err)
                self.finished = "error"
            except pyodbc.ProgrammingError as err:
                self.progress_text = "<font size=5><b> ProgrammingError </b></font><br><br>"+str(err)
                self.finished = "error"
            except pyodbc.NotSupportedError as err:
                self.progress_text = "<font size=5><b> NotSupportedError </b></font><br><br>"+str(err)
                self.finished = "error"
            except pyodbc.DatabaseError as err:
                self.progress_text = "<font size=5><b> DatabaseError </b></font><br><br>"+str(err)
                self.finished = "error"
            except pyodbc.Error as err:
                self.progress_text = "<font size=5><b> Error </b></font><br><br>"+str(err)
                self.finished = "error"
            except:
                self.progress_text = "<font size=5><b> Erro n�o tratado </b></font><br><br>"
                self.finished = "error"
            self.zipar = diretorio+self.backup

        elif self.tipo_banco.currentText() == 'SQL Server CE':
            self.zipar = banco

        elif self.tipo_banco.currentText() == 'Firebird':
            self.zipar = banco

        elif self.tipo_banco.currentText() == 'Oracle':
            try:
                self.cur.execute("CREATE OR REPLACE DIRECTORY backup_zerado AS 'C:\\Backup_Zerado';")
                self.cur.execute("GRANT READ, WRITE ON DIRECTORY backup_zerado TO %s;" % self.user.text())
                self.cur.execute("expdp %s directory=backup_zerado dumpfile=%s.dmp logfile=%s.log" 
                    % (str(self.user.text())+'/'+str(self.pssw.text())+'@'+str(self.server.text()), 
                    (self.cliente.text()).encode('iso-8859-1').decode('iso-8859-1') + "-" + self.data, 
                    (self.cliente.text()).encode('iso-8859-1').decode('iso-8859-1') + "-" + self.data) )
            except cx_Oracle.Warning as err:
                self.progress_text = "<font size=5><b> Warning </b></font><br><br>"+str(err)
                self.finished = "error"
            except cx_Oracle.Error as err:
                self.progress_text = "<font size=5><b> Error </b></font><br><br>"+str(err)
                self.finished = "error"
            except cx_Oracle.InterfaceError as err:
                self.progress_text = "<font size=5><b> InterfaceError </b></font><br><br>"+str(err)
                self.finished = "error"
            except cx_Oracle.DatabaseError as err:
                self.progress_text = "<font size=5><b> DatabaseError </b></font><br><br>"+str(err)
                self.finished = "error"
            except cx_Oracle.DataError as err:
                self.progress_text = "<font size=5><b> DataError </b></font><br><br>"+str(err)
                self.finished = "error"
            except cx_Oracle.OperationalError as err:
                self.progress_text = "<font size=5><b> OperationalError </b></font><br><br>"+str(err)
                self.finished = "error"
            except cx_Oracle.IntegrityError as err:
                self.progress_text = "<font size=5><b> IntegrityError </b></font><br><br>"+str(err)
                self.finished = "error"
            except cx_Oracle.InternalError as err:
                self.progress_text = "<font size=5><b> InternalError </b></font><br><br>"+str(err)
                self.finished = "error"
            except cx_Oracle.ProgrammingError as err:
                self.progress_text = "<font size=5><b> ProgrammingError </b></font><br><br>"+str(err)
                self.finished = "error"
            except cx_Oracle.NotSupportedError as err:
                self.progress_text = "<font size=5><b> NotSupportedError </b></font><br><br>"+str(err)
                self.finished = "error"
            except:
                self.progress_text = "<font size=5><b> Erro n�o tratado! </b></font><br><br>"
                self.finished = "error"
            self.zipar = (self.cliente.text()).encode('iso-8859-1').decode('iso-8859-1') + "-" + self.data + ".dmp"

    def compactaBackup(self):

        zipado = "C:\\Backup_Zerado\\" + (self.revenda.currentText()).encode('iso-8859-1').decode('iso-8859-1') + "-" + self.data + ".zip"

        if exists(zipado):
            try:
                remove(zipado)
            except:
                self.progress_text = "<font size=5><b> Erro ao remover arquivo! </b></font><br><br>\
                    Houve um erro ao remover arquivo j� existente para gerar novo backup.<br>\
                    Verifique na pasta C:\\Backup_Zerado"
                self.finished = "error"

        with ZipFile(zipado, "w") as zipa:

            if self.tipo_banco.currentText() != 'SQL Server' and self.tipo_banco.currentText() != 'Oracle':
                zipar = self.zipar.rstrip('\\')
                zipar = zipar.rstrip('/')
                if zipar.find('/') != -1:
                    zipar = zipar[zipar.rindex('/')+1:]
                elif zipar.find('\\') != -1:
                    zipar = zipar[zipar.rindex('\\')+1:]
                else:
                    pass

                print self.zipar, zipar

                zipa.write(self.zipar, zipar)

            elif self.tipo_banco.currentText() == 'SQL Server':
                zipa.write(self.zipar, self.backup)
            else:
                pass

    def finalizaZeraAcessos(self):

        if self.cnpj.text() != "03.148.451/0001-69":
            query = "insert into acessos_zerados(cliente, cnpj, revenda, sistema, data, motivo, suporte) "
            query += "values("
            query += "'"+self.cliente.text()+"',"
            query += "'"+self.cnpj.text()+"',"
            query += "'"+(self.revenda.currentText()).strip()+"',"
            query += "'"+self.sistema.currentText()+"',"
            query += "'"+(date.today()).strftime("%d/%m/%Y")+"',"
            query += "'"+(self.motivo.toPlainText()).strip()+"',"
            query += "'"+str(gethostname())+"')"

            try:
                mainDB.execute(query)
                connector.commit()
            except pyodbc.OperationalError as err:
                self.progress_text = "<font size=5><b> OperationalError </b></font><br><br>"+str(err)
                self.finished = "error"
            except pyodbc.DataError as err:
                self.progress_text = "<font size=5><b> DataError </b></font><br><br>"+str(err)
                self.finished = "error"
            except pyodbc.IntegrityError as err:
                self.progress_text = "<font size=5><b> IntegrityError </b></font><br><br>"+str(err)
                self.finished = "error"
            except pyodbc.ProgrammingError as err:
                self.progress_text = "<font size=5><b> ProgrammingError </b></font><br><br>"+str(err)
                self.finished = "error"
            except pyodbc.NotSupportedError as err:
                self.progress_text = "<font size=5><b> NotSupportedError </b></font><br><br>"+str(err)
                self.finished = "error"
            except pyodbc.DatabaseError as err:
                self.progress_text = "<font size=5><b> DatabaseError </b></font><br><br>"+str(err)
                self.finished = "error"
            except pyodbc.Error as err:
                self.progress_text = "<font size=5><b> Error </b></font><br><br>"+str(err)
                self.finished = "error"
            except:
                self.progress_text = "<font size=5><b> Erro n�o tratado </b></font><br><br>"
                self.finished = "error"
        else:
            pass

    def layoutWindow(self):
        RevendasZeradas()

class RevendasZeradas(QWidget):
    def __init__(self):
        QWidget.__init__(self)
        table = QTreeView(self)
        
        self.setWindowTitle('Revendas')
        #self.setMinimumSize(600, 400)
        table.setMinimumSize(600, 400)

        cnpjs = (mainDB.execute("select distinct cnpj from acessos_zerados")).fetchall()
        model =  QStandardItemModel(len(cnpjs), 3, table)

        row = 0
        col = 0

        for cnpj in cnpjs:
            row += 1
            item1 = QStandardItem(str(((mainDB.execute("select count(id) from acessos_zerados where cnpj='%s'" % cnpj[0])).fetchall())[0][0]))
            item2 = QStandardItem(str(cnpj[0]))
            item3 = QStandardItem(str(((mainDB.execute("select top(1) cliente from acessos_zerados where cnpj='%s'" % cnpj[0])).fetchall())[0][0]))
            item1.setEditable(False)
            item2.setEditable(False)
            item3.setEditable(False)

            i = 0
            for zerador in (mainDB.execute("select revenda, suporte, motivo from acessos_zerados where cnpj='%s'" % cnpj[0])).fetchall():
                child1 = QStandardItem(zerador[0])
                child2 = QStandardItem(zerador[1])
                child3 = QStandardItem(zerador[2])
                child1.setEditable(False)
                child2.setEditable(False)
                child3.setEditable(False)
                item1.setChild(i,0,child1)
                item1.setChild(i,1,child2)
                item1.setChild(i,2,child3)
                i += 1

            model.setItem(row,col,item1)
            col += 1
            model.setItem(row,col,item2)
            col += 1
            model.setItem(row,col,item3)
            col += 1
            col = 0

        table.setModel(model)
        self.show()
        self.activateWindow()
        self.exec_()

if __name__=='__main__':

    app = QApplication(sys.argv)
    if e != None:
        QMessageBox.critical(None, str(e[0]), str(e[1]))
        sys.exit()
        exit()
    main = MainWindow()
    main.show()
    sys.exit(app.exec_())
    exit()
