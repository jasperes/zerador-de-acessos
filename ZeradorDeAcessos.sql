USE [master]
GO

CREATE DATABASE [ZeradorDeAcessos]
GO

USE [ZeradorDeAcessos]
GO

CREATE TABLE [Acessos_Zerados]
(
	[id] [int] IDENTITY(1,1) PRIMARY KEY NOT NULL,
	[cliente] [varchar](150) NOT NULL,
	[dado] [varchar](18) NOT NULL,
	[revenda] [varchar](50) NOT NULL,
	[sistema] [varchar](30) NOT NULL,
	[data] [datetime] NOT NULL,
	[motivo] [varchar](200) NOT NULL,
	[computador] [varchar](20) NOT NULL
)